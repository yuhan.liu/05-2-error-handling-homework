package com.twuc.webApp.web;

import com.twuc.webApp.Message;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class HandlerExceptionTest {
    @Autowired
    private TestRestTemplate template;

    @Test
    void should_return_message_when_width_is_one() {
        ResponseEntity<Message> forEntity = template.getForEntity("/mazes/color-solve?width=1&height=10", Message.class);
        assertThat(HttpStatus.BAD_REQUEST).isEqualTo(forEntity.getStatusCode());
        assertThat("The width(1) is not valid").isEqualTo(forEntity.getBody().getMessage());
        assertThat(MediaType.APPLICATION_JSON).isEqualTo(forEntity.getHeaders().getContentType());
    }

    @Test
    void should_return_message_when_height_is_one() {
        ResponseEntity<Message> forEntity = template.getForEntity("/mazes/color-solve?width=10&height=1", Message.class);
        assertThat(HttpStatus.BAD_REQUEST).isEqualTo(forEntity.getStatusCode());
        assertThat("The height(1) is not valid").isEqualTo(forEntity.getBody().getMessage());
        assertThat(MediaType.APPLICATION_JSON).isEqualTo(forEntity.getHeaders().getContentType());
    }

    @Test
    void should_return_message_when_type_is_not_true() {
        ResponseEntity<Message> forEntity = template.getForEntity("/mazes/xxx?width=10&height=10", Message.class);
        assertThat(HttpStatus.BAD_REQUEST).isEqualTo(forEntity.getStatusCode());
        assertThat("Invalid type: xxx").isEqualTo(forEntity.getBody().getMessage());
        assertThat(MediaType.APPLICATION_JSON).isEqualTo(forEntity.getHeaders().getContentType());
    }


}
