package com.twuc.webApp.handler;

import com.twuc.webApp.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandlerException {
    private static final Logger logger = LoggerFactory.getLogger(HandlerException.class);

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Message> returnMessageOfError(IllegalArgumentException exception){
        logger.error(exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new Message(exception.getMessage()));
    }
}
